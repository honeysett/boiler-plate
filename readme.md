#Basic HTML Boiler Plate Setup

Only the ***src*** is committed to the git repo.

For development run ***npm start***
This will create a *builds/dev* folder and open the browser with a live update server of the site


For production run ***npm run build***
This will create a *builds/prod* folder with all the files necessary to deploy to prod.  It will also open a live update server to preview the result.